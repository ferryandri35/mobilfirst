module.exports = {
  mode: "jit",
  purge: [
    "./components/**/*.{vue,js}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}"
  ],

  theme: {
    extend: {
      borderWidth: ["hover", "focus"],
      borderRadius: {
        customradius: "12px"
      },
      boxShadow: {
        xl: "0px 3px 6px #00000029"
      },
      colors: {
        greycolor: "#4A5568",
        greyligten: "#F7FAFC",
        greydark: "#718096",
        greenlighten: "#E6FFFA",
        greenlighten1: "#EBF4FF",
        secondarycolor: "#2D3748",
        graylighten1: "#CBD5E0",
        tosca: "#81E6D9",
        greendark: "#81E6D9",
        greydark: "#00000029",
        blue: "#3182CE"
      },
      fontFamily: {
        lato: ["Lato", "sans-serif"]
      },
      width: {
        mobile: "360px"
      }
    }
  }
};
